<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class SearchController extends FrontendController
{
    public function index(Request $request)
    {
        // Lấy danh mục
        $url = $request->segment(2);
        $url = preg_split('/(-)/i', $url);
        // Lấy danh mục
        $cateProduct = [];
        if ($id = array_pop($url)) {
            $cateProduct = Category::findOrFail($id);
        }

        // lấy danh mục con nếu có

        // Danh sách nhà cung cấp
       


        // Lấy sản phẩm
        $products = Product::where("product_active", Product::STATUS_PUBLIC);
        if ($id) {
            $products->where('product_category_id', $id);
        }

        if ($request->k) {
            $products->where('product_name', 'like', '%' . $request->k . '%');
        }

        if ($request->price) {
            $price = $request->price;
            switch ($price) {
                case '1':
                    $products->where('product_price', '<', 1000000);
                    break;

                case '2':
                    $products->whereBetween('product_price', [1000000, 3000000]);
                    break;

                case '3':
                    $products->whereBetween('product_price', [3000000, 5000000]);
                    break;

                case '4':
                    $products->whereBetween('product_price', [5000000, 7000000]);
                    break;

                case '5':
                    $products->whereBetween('product_price', [7000000, 10000000]);
                    break;

                case '6':
                    $products->where('product_price', '>', 10000000);
                    break;
            }
        }

        if ($request->orderby) {
            $orderby = $request->orderby;

            switch ($orderby) {
                case 'desc':
                    $products->orderBy('id', 'DESC');
                    break;

                case 'asc':
                    $products->orderBy('id', 'ASC');
                    break;

                case 'price-max':
                    $products->orderBy('product_price', 'ASC');
                    break;

                case 'price-min':
                    $products->orderBy('product_price', 'DESC');
                    break;
                default:
                    $products->orderBy('id', 'DESC');

            }
        }

        $products = $products->paginate(3);

        $viewData = [
            'products'         => $products,
            'cateProduct'      => $cateProduct, 
            'query'            => $request->query(),
            // 'suppliers'        => $suppliers,
            // 'productHot'       => $productHot ?? null
        ];

        return view('product.index', $viewData);
    }
}
