<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Transactions;
use App\Models\Orders;
use App\Models\Product;
use DB;
class AdminTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        $transactions = Transactions::with('getNameUserTransactions:id,name')->get();
        $viewData = array(
            'transactions' => $transactions,
        );
        
        return view('admin::transaction.index', $viewData);
    }



    
    public function deleteDetailsOrders($id) {
        
        $transaction = Transactions::find($id);
        $transaction->delete();

        return redirect()->back()->with('message', 'Xóa đơn hàng thành công');
    }

    public function viewOrders(Request $request, $id) {
        $ajaxviewOrders = $request->ajax();
        if($ajaxviewOrders) {
            $orders = Orders::with('products')->where('orders_transactions_id', $id)->get();
            $renderHtml = view('admin::components.orders', compact('orders'))->render();
            return response()->json($renderHtml);
        }
    }

    public function actionTransaction($id) {
        $transaction = Transactions::find($id);
		$orders = Orders::where('orders_transactions_id',$id)->get();

		if ($orders)
		{
			//tru di so luong cua san pham
			// tang bien pay san pham
			foreach ($orders as $order)
			{
				$product = Product::find($order->orders_product_id);
				$product->product_number = $product->product_number - $product->orders_qty;
				$product->product_pay ++;
				$product->save();
			}
		}

		// update user
		\DB::table('users')->where('id',$transaction->transactions_user_id)
			->increment('total_pay');

		$transaction->transactions_status = Transactions::STATUS_DONE;
		$transaction->save();

		return redirect()->back()->with('success','Xử lý đơn hàng thành công');
    }
}
