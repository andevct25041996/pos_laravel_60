<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
class ProductDetailsController extends FrontendController
{
    
    public function __construct() {
        parent::__construct();
    }


    public function getProductDetails(Request $request) {

        $url = $request->segment(2);
        $url = preg_split('/(-)/i', $url);
        $id = array_pop($url);
        if($id) {
            $productDetail = Product::findOrFail($id);

            // Danh mục sản phẩm đó
			$cateProduct = Category::find($productDetail->product_category_id);

            // Sản phẩm liên quan
            $productSuggest = Product::where('product_active',Product::STATUS_PUBLIC)
                            ->where('product_category_id', $cateProduct->id)
                            ->orderByDesc('id')
                            ->limit(4)
                            ->get();
            $viewData = array(
                'productSuggest' => $productSuggest,
                'cateProduct'    => $cateProduct,
                'productDetail' => $productDetail
            );
            return view('product.details', $viewData);
        }
        return redirect()->to('/');
    }
}
