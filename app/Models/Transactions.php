<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $table = 'transactions';
    protected $guarded = ['*'];
	
	const STATUS_DONE = 1;
	const STATUS_DEFAULT = 0;
	const TYPE_CART = 1;
	const TYPE_PAY  = 2;

    public function getNameUserTransactions() {
        return $this->belongsTo(User::class, 'transactions_user_id');
    }
}
