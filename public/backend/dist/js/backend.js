$(function(){
    // REMOVE THÔNG BÁO 
    setTimeout(function() {
        $('.alert-success').remove();
    }, 6000);
    /* CHI TIẾT ĐƠN HÀNG */
    $(".js_view_orders").click(function(event){
        event.preventDefault();
        let url = $(this).attr("href");
        $(".modal-body").html('');
        $(".get-transactions-id").text("").text($(this).attr("data-id"));
        $("#staticBackdrop").modal('show');
        $.ajax({
        	url: url,
        }).done(function(result){
        	console.log(result);
        	if(result) {
        		$(".modal-body").append(result);
        	}
        });
	});
	function formatPrice(x) {
		return x.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
	}
    // THỐNG KÊ BIỂU ĐỒ DOANH THU BÁN HÀNG
    let dataJson = $(".dashboard-total-transactions").attr("data-json");
    if (typeof dataJson !== 'undefined') {
		dataChart = JSON.parse(dataJson);
		let price = '{point.y}';
		
		Highcharts.chart('container', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'Biểu đồ doanh thu ngày và tháng '
			},
			xAxis: {
				type: 'category'
			},
			yAxis: {
				title: {
					text: 'Mức độ'
				}

			},
			legend: {
				enabled: false
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: formatPrice(price)
					}
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
				pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y.}%</b> of total<br/>'
			},

			series: [
				{
					name: "Browsers",
					colorByPoint: true,
					data: dataChart
				}
			]
		});
    }
})

