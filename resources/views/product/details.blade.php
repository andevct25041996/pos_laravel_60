@extends('layouts.pos')
@section('content')
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container-inner">
                    <ul>
                        <li class="home">
                            <a href="{{route('home')}}">Trang chủ</a>
                            <span><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="category3"><span>{{$productDetail->product_name}}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-details-area">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="zoomWrapper">
                    <div id="img-1" class="zoomWrapper single-zoom">
                        <a href="#">
                            <div style="height:450px;width:450px;" class="zoomWrapper">
                                <img id="zoom1" src="{{asset(pare_url_file_home($productDetail->product_avatar))}}" data-zoom-image="{{asset(pare_url_file_home($productDetail->product_avatar))}}" alt="big-1"
                                    style="position: absolute;">
                            </div>
                        </a>
                    </div>

                </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="product-list-wrapper">
                    <div class="single-product">
                        <div class="product-content">
                            <h2 class="product-name"><a href="#">{{$productDetail->product_name}}</a></h2>
                            <div class="rating-price">
                                <div class="pro-rating">
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                </div>
                                <div class="price-boxes">
                                    <span class="new-price">{{formatPrice($productDetail->product_price)}}</span>
                                </div>
                            </div>
                            <div class="product-desc">
                                <p>{{$productDetail->product_description}}
                                </p>
                            </div>
                            <p class="availability in-stock">Availability: <span>In stock</span></p>
                            <div class="actions-e">
                                <div class="action-buttons-single">
                                    <div class="inputx-content">
                                        <label for="qty">Số lượng:</label>
                                        <input type="text" name="qty" id="qty" maxlength="12" value="1" title="Qty"
                                            class="input-text qty">
                                    </div>
                                    <div class="add-to-cart">
                                        <a href="{{ route('add.shopping.cart', $productDetail->id) }}">Thêm vào giỏ</a>
                                    </div>

                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="single-product-tab">
                <!-- Nav tabs -->
                <ul class="details-tab">
                    <li class="active"><a href="#home" data-toggle="tab">Chi tiết sản phẩm</a></li>
                    <li class=""><a href="#messages" data-toggle="tab"> Đánh giá</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="product-tab-content">
                            <p>{!! $productDetail->product_content !!}</p>

                        </div>
                    </div>
                    <h3> Đánh giá sản phẩm </h3>
                    <div class="component-ratings">

                        <div class="row">
                            <div class="left-ratings">
                                <div class="ratings-item">
                                    <div class="span-star">
                                        <b>2.5</b>
                                        <span class="class-star fa fa-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="right-ratings">
                                <div class="list-ratings">
                                    @for($i = 1; $i <= 5; $i++) <div class="component-right-ratings">
                                        <div class="i-star">
                                            {{$i}} <span class="fa fa-star"></span>
                                        </div>
                                        <div class="line-before">
                                            <div class="bgb-in" style="width: 28%"></div>
                                        </div>
                                        <div class="total-ratings">
                                            <strong class="strong-number-ratings">119</strong> đánh giá
                                        </div>
                                </div>
                                @endfor
                            </div>
                        </div>
                        <div class="button-ratings">
                            <button type="submit" class="button-ratings-end js-send-ratings">Gửi đánh giá của
                                bạn</button>
                        </div>
                    </div>
                </div>
                <?php 
                        $listRatingsLabel = array(
                            1 => 'Không thích',
                            2 => 'Tạm được',
                            3 => 'Bình thường',
                            4 => 'Rất tốt',
                            5 => 'Tuyệt vời',
                        );
                    ?>
                <div class="form-ratings hide">
                    <div class="choose-ratings">
                        <p class="p-choose-ratings">Chọn đánh giá của bạn</p>
                        <span class="choose-list-ratings">
                            @for ($i = 1; $i <= 5; $i++) <i class="fa fa-star" data-key="{{$i}}"></i>
                                @endfor
                        </span>
                        <span class="title-choose-ratings"></span>
                    </div>
                    <div class="content-ratings">
                        <textarea name="" class="form-control" cols="30" rows="3"></textarea>
                    </div>
                    <div class="button-submit-ratings">
                        <button class="btn btn-primary">Gửi đánh giá</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="our-product-area new-product">
    <div class="container">
        <div class="area-title">
            <h2>Sản phẩm mới</h2>
        </div>
        <!-- our-product area start -->
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="session-products-related">
                        <!-- single-product start -->
                        <div class="products-related">
                            <div class="products-with-related">
                                @foreach($productSuggest as $product)
                                <div class="col-md-3 col-xs-6">

                                    <div class="single-product first-sale">
                                        <div class="product-img">
                                            <a
                                                href="{{ route('get.details.product',[$product->product_slug, $product->id]) }}">
                                                <img class="primary-image"
                                                    src="{{ pare_url_file($product->product_avatar) }}" alt="">
                                                <img class="secondary-image"
                                                    src="{{ pare_url_file($product->product_avatar) }}" alt="">
                                            </a>
                                            <div class="action-zoom">
                                                <div class="add-to-cart">
                                                    <a href="#" title="Quick View"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <div class="action-buttons">
                                                    <div class="add-to-links">
                                                        <div class="add-to-wishlist">
                                                            <a href="{{ route('add.shopping.cart', $product->id) }}"
                                                                title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                        </div>
                                                        <div class="compare-button">
                                                            <a href="#" title="Add to Cart"><i class="icon-bag"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="quickviewbtn">
                                                        <a href="#" title="Add to Compare"><i
                                                                class="fa fa-retweet"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price">
                                                    @if ($product->product_sale)
                                                    {{ number_format($product->product_price * (100 - $product->product_sale) / 100,0,',','.') }}
                                                    VNĐ
                                                    <del
                                                        class="product-old-price">{{ number_format($product->product_price,0,',','.') }}</del>
                                                    VNĐ
                                                    @else
                                                    {{formatPrice($product->product_price)}}
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <h2 class="product-name">
                                                <a
                                                    href="{{ route('get.details.product',[$product->product_slug, $product->id]) }}">{{$cateProduct->categories_name}}</a>
                                            </h2>
                                            <p class="products-details-name">{{ $product->product_name }}</p>
                                        </div>
                                    </div>

                                </div>
                                @endforeach
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
        <!-- our-product area end -->
    </div>
</div>
@endsection