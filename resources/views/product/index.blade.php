@extends('layouts.pos')
@section('content')
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container-inner">
                    <ul>
                        <li class="home">
                            <a href="{{route('home')}}">Trang chủ</a>
                            <span><i class="fa fa-angle-right"></i></span>
                        </li>
                        @if (isset($cateProduct) && !empty($cateProduct))
                        <li class="category3"><span>{{$cateProduct->categories_name}}</span></li>
						@elseif(Route::current()->getName() == 'get.search')
						<li>Tìm kiếm</li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumbs area end -->
<!-- shop-with-sidebar Start -->
<div class="shop-with-sidebar">
    <div class="container">
        <div class="row">
            <!-- left sidebar start -->
            <div class="col-md-3 col-sm-12 col-xs-12 text-left">
                <div class="topbar-left">
                    <aside class="widge-topbar">
                        <div class="bar-title">
                            <div class="bar-ping"><img src="img/bar-ping.png" alt=""></div>
                            <h2>Lọc điều kiện</h2>
                        </div>
                    </aside>
                    <aside class="sidebar-content">
                        <div class="sidebar-title">
                            <h6>Khoảng giá</h6>
                        </div>
                        <ul class="sidebar-tags">
                            <li><a class="{{ Request::get('price') == 1 ? 'active' : ''}}"
                                    href="{{request()->fullUrlWithQuery(['price' => 1])}}">Dưới 1 triệu</a></li>
                            <li><a class="{{ Request::get('price') == 2 ? 'active' : ''}}"
                                    href="{{request()->fullUrlWithQuery(['price' => 2])}}">1 triệu - 3 triệu</a></li>
                            <li><a class="{{ Request::get('price') == 3 ? 'active' : ''}}"
                                    href="{{request()->fullUrlWithQuery(['price' => 3])}}">3 triệu - 5 triệu</a></li>
                            <li><a class="{{ Request::get('price') == 4 ? 'active' : ''}}"
                                    href="{{request()->fullUrlWithQuery(['price' => 4])}}">5 triệu - 7 triệu</a></li>
                            <li><a class="{{ Request::get('price') == 5 ? 'active' : ''}}"
                                    href="{{request()->fullUrlWithQuery(['price' => 5])}}">Trên 10 triệu</a></li>
                        </ul>
                    </aside>

                </div>
            </div>
            <!-- left sidebar end -->
            <!-- right sidebar start -->
            <div class="col-md-9 col-sm-12 col-xs-12">
                <!-- shop toolbar start -->
                <div class="shop-content-area">
                    <div class="shop-toolbar">
                        <div class="col-md-4 col-sm-4 col-xs-12 nopadding-left text-left">
                            <form class="tree-most" method="get" id="select-sort-products">
                                <div class="orderby-wrapper">
                                    <label>Sắp xếp</label>
                                    <select name="orderby" class="orderby">
                                        <option
                                            {{ Request::get('orderby') == "default-product" ? "selected='selected'" : "" }}
                                            value="default-product" selected="selected">Mặc định</option>
                                        <option {{ Request::get('orderby') == "desc" ? "selected='selected'" : "" }}
                                            value="desc">Sản phẩm mới nhất</option>
                                        <option {{ Request::get('orderby') == "asc" ? "selected='selected'" : "" }}
                                            value="asc">Sản phẩm cũ nhất</option>
                                        <option
                                            {{ Request::get('orderby') == "price-min" ? "selected='selected'" : "" }}
                                            value="price-min">Giá giảm dần</option>
                                        <option
                                            {{ Request::get('orderby') == "price-max" ? "selected='selected'" : "" }}
                                            value="price-max">Giá tăng dần</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- shop toolbar end -->
                <!-- product-row start -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="shop-grid-tab">
                        <div class="row">
							
                            <div class="shop-product-tab first-sale">
							@if (isset($products) && !empty($products))
                                @foreach($products as $product)
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="two-product">
                                        <!-- single-product start -->
                                        <div class="single-product">
                                            @if($product->product_number == 0)
                                            <span>Tạm hết hàng</span>
                                            @endif
                                            @if($product->product_sale)
                                            <span class="sale-text">{{$product->product_sale}}</span>
                                            @endif

                                            <div class="product-img">
                                                <a
                                                    href="{{route('get.details.product', [$product->product_slug, $product->id])}}">
                                                    <img class="primary-image"
                                                        src="{{asset(pare_url_file_home($product->product_avatar))}}"
                                                        alt="" />
                                                    <img class="secondary-image"
                                                        src="{{asset(pare_url_file_home($product->product_avatar))}}"
                                                        alt="" />
                                                </a>
                                                <div class="action-zoom">
                                                    <div class="add-to-cart">
                                                        <a href="#" title="Quick View"><i
                                                                class="fa fa-search-plus"></i></a>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div class="action-buttons">
                                                        <div class="add-to-links">
                                                            <div class="add-to-wishlist">
                                                                <a href="#" title="Add to Wishlist"><i
                                                                        class="fa fa-heart"></i></a>
                                                            </div>
                                                            <div class="compare-button">
                                                                <a href="#" title="Add to Cart"><i
                                                                        class="icon-bag"></i></a>

                                                            </div>
                                                        </div>
                                                        <div class="quickviewbtn">
                                                            <a href="#" title="Add to Compare"><i
                                                                    class="fa fa-retweet"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="price-box">
                                                    <span
                                                        class="new-price">{{formatPrice($product->product_price)}}</span>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <h2 class="product-name">
                                                    <a
                                                        href="{{route('get.details.product', [$product->product_slug, $product->id])}}">{{$product->product_name}}</a>
                                                </h2>
                                                <p>{{$product->product_description}}</p>
                                            </div>
                                        </div>
                                        <!-- single-product end -->
                                    </div>
                                </div>
                                @endforeach
								@else
							<h1 class="">Tìm kiếm không thấy</h1>
							@endif
                            </div>
							
                        </div>

                    </div>
                   
                    <!-- shop toolbar start -->
                    <div class="shop-content-bottom">
                        <div class="shop-toolbar btn-tlbr">

                            <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                                <div class="pages">
                                    {!! $products->links() !!}
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- shop toolbar end -->
                </div>
            </div>
            <!-- right sidebar end -->
        </div>
    </div>
</div>
<!-- shop-with-sidebar end -->
<!-- Brand Logo Area Start -->
<div class="brand-area">
    <div class="container">
        <div class="row">
            <div class="brand-carousel">
                <div class="brand-item"><a href="#"><img src="img/brand/bg1-brand.jpg" alt="" /></a></div>
                <div class="brand-item"><a href="#"><img src="img/brand/bg2-brand.jpg" alt="" /></a></div>
                <div class="brand-item"><a href="#"><img src="img/brand/bg3-brand.jpg" alt="" /></a></div>
                <div class="brand-item"><a href="#"><img src="img/brand/bg4-brand.jpg" alt="" /></a></div>
                <div class="brand-item"><a href="#"><img src="img/brand/bg5-brand.jpg" alt="" /></a></div>
                <div class="brand-item"><a href="#"><img src="img/brand/bg2-brand.jpg" alt="" /></a></div>
                <div class="brand-item"><a href="#"><img src="img/brand/bg3-brand.jpg" alt="" /></a></div>
                <div class="brand-item"><a href="#"><img src="img/brand/bg4-brand.jpg" alt="" /></a></div>
            </div>
        </div>
    </div>
</div>
@endsection