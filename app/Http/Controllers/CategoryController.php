<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
class CategoryController extends FrontendController
{
    public function getListProduct(Request $request) {
        $url = $request->segment(2);
        $url = preg_split('/(-)/i', $url);

        if ($id = array_pop($url)) {
            $products = Product::where([
                'product_category_id' => $id,
                'product_active' => Product::STATUS_PUBLIC,
            ]);
            
            // sort price min to max

            if($request->price) {
                $price = $request->price;
                switch ($price) {
                    case '1':
                        $products->where('product_price','<','1000000');
                        break;
                    case '2':
                        $products->whereBetween('product_price',[1000000,3000000]);
                        break;
                    case '3':
                        $products->whereBetween('product_price',[3000000,5000000]);
                        break;
                    case '4':
                        $products->whereBetween('product_price',[5000000,7000000]);
                        break;
                    case '5':
                        $products->where('product_price','>','10000000');
                        break;
                    default:
                        $products->where('product_price','<','1000000');
                    break;
                }
            }

            // sort orderby
            $orderBy = $request->orderby;
            if($orderBy) {
                switch ($orderBy) {
                    case 'desc':
                        $products->orderBy('id', 'DESC');
                        break;
                    case 'asc':
                        $products->orderBy('id', 'ASC');
                        break;
                    case 'price-min':
                        $products->orderBy('product_price', 'DESC');
                        break;
                    case 'price-max':
                        $products->orderBy('product_price', 'ASC');
                        break;
                    default:
                        $products->orderBy('id', 'DESC');
                        break;
                }
            }

            $products = $products->orderBy('id', 'DESC')->paginate(3);
            
            
            
            $cateProduct = Category::find($id);
            $viewData = array(
                'products' => $products,
                'cateProduct' => $cateProduct,
            );

            return view('product.index', $viewData);
        }
    }
}
