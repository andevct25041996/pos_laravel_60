<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::group(['namespace' => 'Auth'], function () {
    
    Route::get('register', 'RegisterController@getRegister')->name('get.register');
    Route::post('register', 'RegisterController@postRegister')->name('post.register');
    
    Route::get('login', 'LoginController@getLogin')->name('get.login');
    Route::post('login', 'LoginController@postLogin')->name('post.login');

    Route::get('logout', 'LogoutController@getLogout')->name('get.logout');
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('tim-kiem','SearchController@index')->name("get.search");
Route::get('/san-pham/{slug}-{id}', 'CategoryController@getListProduct')->name('get.list.product');
Route::get('san-pham','CategoryController@getListProduct')->name('get.product.list');
Route::get('/chi-tiet-san-pham/{slug}-{id}', 'ProductDetailsController@getProductDetails')->name('get.details.product');

// bai viet
Route::get('tin-tuc', 'PostsController@getListPosts')->name('get.list.posts');
Route::get('tin-tuc/{slug}-{id}','PostsController@getPostsDetails')->name('get.details.posts');

// lien-he
Route::get('lien-he', 'ContactController@getContact')->name('get.contact');
Route::post('lien-he', 'ContactController@postContact')->name('post.contact');

// them-san-pham-gio-hang
Route::group(['prefix' => 'shopping'], function () {
    Route::get('/add/{id}', 'ShoppingCartController@addProduct')->name('add.shopping.cart');
    Route::get('/delete/{id}', 'ShoppingCartController@deleteProduct')->name('detele.shopping.cart');
    Route::get('/danh-sach', 'ShoppingCartController@getListProductShopping')->name('get.list.shopping.cart');
    Route::get('/update/{id}','ShoppingCartController@updateShoppingCart')->name('updateShoppingCart');
    
});
// thanh-toan
Route::group(['prefix' => 'gio-hang','middleware' => 'CheckLoginUser'], function () {
    Route::get('/thanh-toan', 'ShoppingCartController@getCheckout')->name('get.checkout.shopping.cart');
    Route::post('/thanh-toan', 'ShoppingCartController@saveInfoShoppingCart');
    Route::get('/thank-you', 'ShoppingCartController@getThankyou')->name('get.thankyou.shopping.cart');
});


/* Dashboard User */
Route::group(['prefix' => 'users','middleware' => 'CheckLoginUser'], function () {
    Route::get('/', 'UserController@index')->name('user.dashboard.index');
    Route::get('/info','UserController@updateInfo')->name('user.update.info');
    Route::post('/info','UserController@saveUpdateInfo');
    Route::get('/password','UserController@updatePassword')->name('user.update.password');
    Route::post('/password','UserController@saveUpdatePassword');
    Route::get('giao-dich','UserController@getHistoryTransactions')->name('user.transaction_history');
    Route::get('/san-pham-ban-chay','UserController@getProductPay')->name('user.list.product');
});