<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Category;
use App\Models\Setting;
// use App\Models\Post;
class FrontendController extends Controller
{
    
    public function __construct()
    {
        $categories = Category::all();
        View::share('categories', $categories);
        $setting = Setting::first();
        View::share('setting', $setting);
        $categoriesAll = Category::where('categories_active',Category::STATUS_PUBLIC)
                ->get();
        View::share('categoriesAll', $categoriesAll);
    }
    
}
