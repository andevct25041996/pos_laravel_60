@if (isset($listPosts))
    @foreach($listPosts as $post)
        <div class="article">
            <div class="article_avatar">
                <a href="{{ route('get.details.posts',[$post->post_slug,$post->id]) }}">
                    <img src="{{asset(pare_url_file_home($post->post_avatar))}}"alt="{{ $post->post_name }}">
                </a>
            </div>
            <div class="article_info">
                <h2><a href="{{ route('get.details.posts',[$post->post_slug,$post->id]) }}">{{ $post->post_name }}</a></h2>
                <p>{{ $post->post_description }}</p>
                <p>Admin <span>{{ $post->created_at }}</span></p>
            </div>
        </div>
    @endforeach
    {!! $listPosts->links() !!}
@endif
