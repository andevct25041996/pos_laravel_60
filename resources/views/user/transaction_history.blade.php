@extends('layouts.pos')
@section('content')
    <div class="our-product-area new-product">
        <div class="container">
            <div class="area-title">
                <h2 style="margin-top: 20px">Lịch sử giao dịch</h2>
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Thông tin khách hàng</th>
                    <th>Tổng Tiền</th>
                    <th>Trạng thái</th>
                    
                </tr>
                </thead>
                <tbody>
                @foreach($transactions as $transaction)
                    <tr>
                        <td>#{{ $transaction->id }}</td>
                        <td>
                            <p><b style="display: inline-block;width: 70px">Tên khách hàng </b>{{ isset($transaction->getNameUserTransactions->name) ? $transaction->getNameUserTransactions->name : '[N\A]' }}</p>
                            <p><b style="display: inline-block;width: 70px">Địa chỉ </b>{{ $transaction->transactions_address }}</p>
                            <p><b style="display: inline-block;width: 70px">Số điện thoại </b>{{ isset($transaction->getNameUserTransactions->phone) ? $transaction->getNameUserTransactions->phone : "[N\A]" }}</p>
                            <p><b style="display: inline-block;width: 70px">Thời gian </b>{{ $transaction->created_at->format('d-m-Y') }}</p>
                        </td>
                        <td>{{ number_format($transaction->transactions_total,0,',','.') }} VNĐ</td>
                        <td>
                            @if ( $transaction->transactions_status == 1)
                                <a href="#" class="label-success label">Đã xử lý</a>
                            @else
                                <a href="#" class="label label-default">Chờ xử lý</a>
                            @endif
                        </td>
                       
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
                {!! $transactions->links() !!}
            </div>
        </div>
    </div>
@stop
