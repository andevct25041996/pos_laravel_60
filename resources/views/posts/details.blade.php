@extends('layouts.pos')
@section('content')
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container-inner">
                    <ul>
                        <li class="home">
                            <a href="{{route('home')}}">Trang chủ</a>
                            <span><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="category3"><span>Tin tức</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-contact-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="article_content" style="margin-bottom: 20px">
                    <h1>{{ $postsDetail->post_name }}</h1>
                    <p style="font-weight: 500;color: #333">{{ $postsDetail->post_description }}</p>
                    <div>
                            {!! $postsDetail->post_content !!}
                    </div>
                </div>
                <h4>Bài viết khác</h4>
                @include('components.posts')
            </div>
            <div class="col-sm-3">
                <h5>Bài viết nổi bật</h5>
                <div class="list_article_hot">
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection