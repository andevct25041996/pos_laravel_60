<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Post;
use App\Models\Contact;
use App\Models\Product;
use App\Models\Transactions;
use App\Models\Category;
use App\User;
use App\Models\Banner;
use App\Models\Orders;
use Carbon\Carbon;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $countOrders = Orders::count();
        $countBanner = Banner::count();
        $countCategory = Category::count();
        $countContacts = Contact::count();
        $contacts = Contact::limit(10)->get();
		$countUser = User::count();
		$countProduct = Product::count();
        $countPost = Post::count();

        // doanh thu ngay
		$moneyDay = Transactions::whereDay('created_at',date('d'))
        ->where('transactions_status',Transactions::STATUS_DONE)
        ->sum('transactions_total');
        
        
    
        $mondayLast = Carbon::now()->startOfWeek();
        $sundayFirst = Carbon::now()->endOfWeek();
        $moneyWeed = Transactions::whereBetween('created_at',[$mondayLast,$sundayFirst])
            ->where('transactions_status',Transactions::STATUS_DONE)
            ->sum('transactions_total');
        
        // doanh thu thag
        $moneyMonth = Transactions::whereMonth('created_at',date('m'))
            ->where('transactions_status',Transactions::STATUS_DONE)
            ->sum('transactions_total');
        
        // doanh thu nam
        $moneyYear = Transactions::whereYear('created_at',date('Y'))
            ->where('transactions_status',Transactions::STATUS_DONE)
            ->sum('transactions_total');
        
        $dataMoney = [
            [
                "name" => "Doanh thu ngày",
                "y"    => (int)$moneyDay
            ],
            [
                "name" => "Doanh thu tuần",
                "y"    => (int)$moneyWeed
            ],
            [
                "name" => "Doanh thu tháng",
                "y"    => (int)$moneyMonth
            ],
            [
                "name" => "Doanh thu năm",
                "y"    => (int)$moneyYear
            ]
        ];

       
        // danh sach don hang moi
        
        $transactionNews = Transactions::with('getNameUserTransactions:id,name')
            ->limit(5)
            ->orderByDesc('id')
            ->get();
        
        $viewData = [
            'countOrders' => $countOrders,
            'countBanner' => $countBanner,
            'countCategory' => $countCategory,
			'contacts' => $contacts,
			'countContacts'   => $countContacts,
			'countUser'       => $countUser,
			'countProduct'    => $countProduct,
            'countPost'       => $countPost,
            'moneyDay'        => $moneyDay,
			'moneyMonth'      => $moneyMonth,
			'dataMoney'       => json_encode($dataMoney),
			'transactionNews' => $transactionNews,
		];
        
        return view('admin::index', $viewData);
    }

    
}
