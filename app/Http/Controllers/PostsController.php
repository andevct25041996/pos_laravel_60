<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
class PostsController extends FrontendController
{
    public function getListPosts() {
        $listPosts = Post::paginate(10);
        return view('posts.index', compact('listPosts'));
    }

    public function getPostsDetails(Request $request)
	{
		$arrayUrl = (preg_split("/(-)/i",$request->segment(2)));
		
		$id = array_pop($arrayUrl);
		
		if ($id)
		{
			$postsDetail = Post::find($id);
			$posts = Post::orderBy("id","DESC")->paginate(10);

			
			$viewData = [
				'posts' => $posts,
				'postsDetail' => $postsDetail,
			];
			
			return view('posts.details',$viewData);
		}
		
		return redirect('/');
	}
}
