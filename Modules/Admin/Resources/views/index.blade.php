@extends('admin::layouts.master')
@section('title')
    Trang quản trị
@stop
@section('content')
<div class="content-wrapper" style="min-height: 115px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Thống kê doanh số bán hàng</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
                        <li class="breadcrumb-item active">Dashboard v2</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1">
                            <i class="fas fa-id-card-alt"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Tổng liên hệ</span>
                            <span class="info-box-number">{{$countContacts}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1">
                            <i class="nav-icon fas fa-th"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Tổng danh mục</span>
                            <span class="info-box-number">{{ $countCategory}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1">
                            <i class="fas fa-image"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Tổng banner</span>
                            <span class="info-box-number">{{ $countBanner}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1">
                            <i class="fas fa-money-bill"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Tổng đơn hàng</span>
                            <span class="info-box-number">{{ $countOrders}}</span>
                        </div>
                    </div>
                </div>
                
               
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1">
                            <i class="nav-icon fa fa-fw fa-cubes"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Tổng sản phẩm</span>
                            <span class="info-box-number">{{$countProduct}}</span>
                        </div>
                    </div>
                </div>

                <div class="clearfix hidden-md-up"></div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1">
                            <i class="nav-icon fa fa-fw fa-paint-brush"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Tổng bài viết</span>
                            <span class="info-box-number">{{$countPost}}</span>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1">
                            <i class="fas fa-users"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Tổng số thành viên</span>
                            <span class="info-box-number">{{$countUser}}</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row list-orders-dashboard">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header border-transparent">
                            <h3 class="card-title">Danh sách đơn hàng mới nhất</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tên khách hàng</th>
                                            <th>Số điện thoại</th>
                                            <th>Tổng tiền</th>
                                            <th>Trạng thái</th>
                                            <th>Thời gian</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($transactionNews as $transaction)
                                            <tr>
                                                <td>#{{ $transaction->id }}</td>
                                                <td>{{ isset($transaction->getNameUserTransactions->name) ? $transaction->getNameUserTransactions->name : '[N\A]' }}</td>
                                                <td>{{ $transaction->transactions_phone }}</td>
                                                <td>{{ formatPrice($transaction->transactions_total) }}</td>
                                                <td>
                                                    @if ( $transaction->transactions_status == 1)
                                                        <a href="#" class="btn btn-success btn-sm">Đã xử lý</a>
                                                    @else
                                                        <a href="#" class="btn btn-warning btn-sm">Chờ xử lý</a>
                                                    @endif
                                                </td>
                                                <td>{{date('Y-m-d', strtotime($transaction->created_at))}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <a href="{{ route('admin.get.list.transaction') }}" class="btn btn-sm btn-info float-left">Xem tổng đơn hàng</a>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <div class="row list-orders-dashboard">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header border-transparent">
                            <h3 class="card-title">Danh sách liên hệ mới nhất</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tiêu đề</th>
                                            <th>Họ tên</th>
                                            <th>Nội dung</th>
                                            <th>Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($contacts))
                                            @foreach($contacts as $contact)
                                                <tr>
                                                    <td>{{ $contact->id }}</td>
                                                    <td>{{ $contact->contact_subject }}</td>
                                                    <td>{{ $contact->contact_name }}</td>
                                                    <td>{{ $contact->contact_content }}</td>
                                                    <td>
                                                        @if ( $contact->contact_status == 1)
                                                            <span class="label label-success">Đã xử lý</span>
                                                        @else
                                                            <span class="label label-default">Chưa xử lý</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <a href="#" class="btn btn-sm btn-info float-left">Xem danh sách liên hệ</a>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
            <div class="row list-orders-dashboard">
                <div class="col-md-12">
                    <div id="container" class="dashboard-total-transactions col-md-12" data-json="{{$dataMoney}}"></div>
                </div>
            </div>   
             
        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->
</div>
@stop
