<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestPassword;
use App\Models\Product;
use App\Models\Transactions;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index() {
        $transactions     = Transactions::where('transactions_user_id', get_data_user('web'));
        $listTransactions = $transactions;

        $transactions         = $transactions->addSelect('id', 'transactions_total', 'transactions_address', 'transactions_phone', 'created_at', 'transactions_status')->paginate(10);
        $totalTransaction     = $listTransactions->select('id')->count();
        $totalTransactionDone = $listTransactions->where('transactions_status', Transactions::STATUS_DONE)
            ->select('id')
            ->count();

        $viewData = [
            'totalTransaction'     => $totalTransaction,
            'totalTransactionDone' => $totalTransactionDone,
            'transactions'         => $transactions
        ];

        return view('user.index', $viewData);
    }

    public function updateInfo()
    {
        $user = User::find(get_data_user('web'));
        return view('user.info', compact('user'));
    }
    /**
     * luu thong tin
     */
     public function saveUpdateInfo(Request $request)
     {
         User::where('id', get_data_user('web'))
             ->update($request->except('_token'));
 
         return redirect()->back()->with('success', 'Cập nhật thông tin thành công');
     }

     public function updatePassword()
    {
        return view('user.password');
    }

    public function saveUpdatePassword(RequestPassword $requestPassword)
    {
        if (Hash::check($requestPassword->password_old, get_data_user('web', 'password'))) {
            $user           = User::find(get_data_user('web'));
            $user->password = bcrypt($requestPassword->password);
            $user->save();

            return redirect()->back()->with('success', 'Cập nhật thành công');
        }

        return redirect()->back()->with('danger', 'Mật khẩu cũ không đúng');
    }

    // lich su giao dich 
    public function getHistoryTransactions()
    {
        $transactions = Transactions::with('getNameUserTransactions')
            ->where('transactions_user_id', get_data_user('web'))
            ->orderByDesc('id')->paginate(10);

        $viewData     = [
            'transactions' => $transactions
        ];

        return view('user.transaction_history', $viewData);
    }
    // san pham ban chay
    public function getProductPay()
    {
        $products = Product::orderBy('product_pay', 'DESC')->limit(10)->get();
        return view('user.product', compact('products'));
    }

}
